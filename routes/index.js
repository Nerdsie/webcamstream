var express = require('express');
var router = express.Router();
var childProcess = require('child_process');
const fetch = require('node-fetch');

/* GET home page. */
router.get('/agora', async function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');

  try {
    const result = await fetch("http://localhost:8080/agora");
    const body = await result.text()
    res.send(body);
  }catch (error){
    res.status(503);
    res.send('Agora information not available');
    console.log(error)
  }
});

module.exports = router;
